package uniquindio.edu.co.hanoi;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.Serializable;
import java.util.ArrayList;

/**
 * Clase que se encarga de la logica del juego torres de hanoi
 *
 * @author Mateo Cano Alfonso <mcanoa@uqvirtual.edu.co>
 */
public class TorresDeHanoi implements Serializable {

    int nume;

    public void setNume(int nume) {
        this.nume = nume;
        contador++;
    }

    String cadena;
    ArrayList<String> cadenas;
    //guarda el numero de pasos completos
    int contador;

    public TorresDeHanoi() {
        cadena = "";
        cadenas = new ArrayList<>();
        contador = 0;
    }

    public void hanoi(int num, int inicio, int auxiliar, int fin) throws IOException {
        if (num == 1) {
            cadena = "Paso # " + contador + "MOVER DE LA TORRE " + inicio + " A LA TORRE " + fin;
            contador++;
            cadenas.add(cadena);

        } else {
            hanoi(num - 1, inicio, fin, auxiliar);
            cadena = "Paso # " + contador + "MOVER DE LA TORRE " + inicio + " A LA TORRE " + fin;
            contador++;
            cadenas.add(cadena);

            hanoi(num - 1, auxiliar, inicio, fin);
        }
    }

    public void guardarSolucion(File guarda) throws IOException {
        contador--;
        PrintWriter salida = null;
        try {
            FileWriter fileWriter = new FileWriter(guarda + ".txt");
            salida = new PrintWriter(fileWriter);
            salida.println("Sulocion de tore para " + nume + " arros.\n");

            salida.println("Torre solucionada con " + contador + " pasos..\n");
            salida.println();
            for (String cad : cadenas) {
                salida.println(cad);
                salida.println();
            }
salida.println("------------------------Torre solucionada FIN.-----------------------------    \n");
            salida.println();
        } finally {
            if (salida != null) {
                salida.close();
            }
        }

    }

    /**
     * Meotodo para verificar si ya acabo el juego
     *
     * @param n
     * @param numeroArosTorre3
     * @return
     */
    public boolean verificarFinalJuego(int n, int numeroArosTorre3) {

        return n == numeroArosTorre3;
    }

    /**
     * Metodo que retorna el minimo de movimientos segun el numero de aros
     *
     * @return
     */
    public int getContador() {
        return contador;
    }

    /**
     * Metodo para setear el contador
     *
     * @param contador
     */
    public void setContador(int contador) {
        this.contador = contador;
    }

}
