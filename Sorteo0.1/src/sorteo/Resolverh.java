/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sorteo;

import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;

/**
 *
 * @author Clite
 */
public class Resolverh {

    int num = 0;
    String cadena;
    ArrayList<String> cadenas;
    //guarda el numero de pasos completos
    int contador;

    public Resolverh() {
        cadena = "";
        cadenas = new ArrayList<>();
        contador = 1;

    }

    public void setNum(int num) {
        this.num = num;
    }

    public void Torres(int Discos, int torre1, int torre2, int torre3) {
        if (Discos == 1) {
            //System.out.println("Muver Disco de la torre " + torre1 + " a la torre" + torre3);
            cadena = "Paso # " + contador +" Muver Disco de la torre " + torre1 + " a la torre" + torre3;
            contador++;
            cadenas.add(cadena);
        } else {
            Torres(Discos - 1, torre1, torre3, torre2);
            //System.out.println("Muver Disco de la torre " + torre1 + " a la torre" + torre3);
            cadena = "Paso # " + contador +" Muver Disco de la torre " + torre1 + " a la torre" + torre3;
            contador++;
            cadenas.add(cadena);
            Torres(Discos - 1, torre2, torre1, torre3);

        }

    }

    public void guardarSolucion() throws IOException {
        contador--;
        PrintWriter salida = null;
        try {
            FileWriter fileWriter = new FileWriter(num + "Aros resuelto.txt");
            salida = new PrintWriter(fileWriter);
            salida.println("Sulocion de tore para " + num + " arros.\n");

            salida.println("Torre solucionada con "+contador + " pasos..\n");
            salida.println();
            for (String cad : cadenas) {
                salida.println(cad);
                salida.println();
            }

        } finally {
            if (salida != null) {
                salida.close();
            }
        }

    }

}
